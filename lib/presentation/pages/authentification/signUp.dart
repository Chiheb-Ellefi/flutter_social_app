import 'package:flutter/material.dart';

import 'package:my_project/presentation/widgets/authentication/firstSignUp.dart';

class SignUp extends StatelessWidget {
  const SignUp({super.key});

  @override
  Widget build(BuildContext context) {
    return const FirstSignUp();
  }
}
