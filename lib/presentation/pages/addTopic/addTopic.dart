import 'package:flutter/material.dart';
import 'package:my_project/presentation/widgets/addTopic/addTopicWidget.dart';

class CreateTopic extends StatelessWidget {
  const CreateTopic({super.key});

  @override
  Widget build(BuildContext context) {
    return const CreateTopicWidget();
  }
}
